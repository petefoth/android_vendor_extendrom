# FORMAT (this is 1-line but splitted here for better readability):
#
#   <filename-for-download-uri>|
#   <download-uri-without-filename>|
#   <install-path>|
#   <target-filename>;<PRESIGNED or path to certs>|
#   <true|false> to verify GPG signature or not|   --> note: verifying GPG signatures requires that a filename with .asc extension is on the specified repo available
#   LOCAL_OVERRIDES_PACKAGES delimited by ";"|
#   LOCAL_REQUIRED_MODULES delimited by ";"
#
# set the above in 1 line like this:
#<filename-for-download-uri>|<download-uri-without-filename>|<install-path>|<target-filename>;<PRESIGNED or path to certs>|<true|false>|LOCAL_OVERRIDES_PACKAGES|LOCAL_REQUIRED_MODULES
#
# special keywords
#    - FDROIDREPO (download-uri): will use the f-droid repo (or mirror) specified in get_prebuilts.sh
#    - LATEST (filename. replacing a version number of an APK): instead of a specific version fetch the latest version (works for FDROID only)
#      e.g. org.fdroid.fdroid_1012003.apk becomes org.fdroid.fdroid_LATEST.apk
#
# Examples:
#    itsme_LATEST.apk|FDROIDREPO|app|itsme.apk;user-keys/shared|true|Launcher;Home|libfoo;libmaa|
#    itsme.apk|http://dlurl.local|app|itsme.apk;PRESIGNED|false|Launcher912;Home|libfoo;libmaa|

##########################################################################################################
# SYSTEM
##########################################################################################################

# F-Droid
org.fdroid.fdroid_LATEST.apk|FDROIDREPO|app|F-Droid.apk;PRESIGNED|true

# F-Droid privileged extension should be build with AOSP but if you REALLY wanna use a prebuilt:
org.fdroid.fdroid.privileged_LATEST.apk|FDROIDREPO|priv-app|F-DroidPrivilegedExtension_pb.apk;PRESIGNED|true

# AuroraStore
com.aurora.store_LATEST.apk|FDROIDREPO|app|AuroraStore.apk;PRESIGNED|true

# Aurora Services
# Allow Aurora Store to install updates automatically
AuroraServices_v1.1.1.apk|https://gitlab.com/AuroraOSS/AuroraServices/uploads/c22e95975571e9db143567690777a56e/|priv-app|AuroraServices.apk;PRESIGNED|false||permissions_com.aurora.services.xml


# AdAway
org.adaway_LATEST.apk|FDROIDREPO|priv-app|AdAway.apk;PRESIGNED|false

# DNS66
org.jak_linux.dns66_LATEST.apk|FDROIDREPO|app|DNS66.apk;PRESIGNED|true

# Magisk
Magisk-v25.2.apk|https://github.com/topjohnwu/Magisk/releases/download/v25.2|app|Magisk.zip;PRESIGNED|false
Magisk-v25.2.apk|https://github.com/topjohnwu/Magisk/releases/download/v25.2|app|SignMagisk.zip;user-keys/shared|false

# Google in-app purchase support by nanolx (https://gitlab.com/Nanolx/microg-phonesky-iap-support)
Phonesky_132.apk|https://nanolx.org/fdroid/repo|priv-app|Phonesky.apk;PRESIGNED|false|FakeStore|phonesky-permissions.xml;com.android.vending.xml

# MicrogGmsCore
com.google.android.gms-LATEST.apk|https://microg.org/fdroid/repo/|priv-app|MicrogGmsCore.apk;PRESIGNED|false|GmsCore|microg.xml;privapp-permissions-com.google.android.gms.xml

# OpenCamera
net.sourceforge.opencamera_LATEST.apk|FDROIDREPO|app|OpenCamera.apk;PRESIGNED|true

##########################################################################################################
# LAUNCHERS
##########################################################################################################

# Omega Launcher
Omega.v0.8.0.Build.168.apk|https://github.com/otakuhqz/Omega/releases/download/v0.8.0|app|Omega.apk;PRESIGNED|false

# Lawnchair - stable
# taken from PlayStore via AuroraStore:
Lawnchair_202589.apk|https://www.dropbox.com/s/qi551e1y2ubd1zd|app|Lawnchair-stable.apk;PRESIGNED|false

# Lawnchair - latest
# taken directly from the official Lawnchair TG group (https://t.me/lawnchairci)
Lawnchair_latest.apk|https://leech.binbash.rocks:8008/misc|app|Lawnchair-latest.apk;PRESIGNED|false

# OpenLauncher
com.benny.openlauncher_LATEST.apk|FDROIDREPO|app|OpenLauncher.apk;PRESIGNED|true

##########################################################################################################
# INTERNET
##########################################################################################################

# K9-Mail
# latest version has no IMAP-idle / push
# Earlier version 5.600 *with* IMAP-idle / push is available in F-Droid 
# (if the F-Droid Archive repo is enabled)
com.fsck.k9_LATEST.apk|FDROIDREPO|app|K9-Mail-latest.apk;PRESIGNED|true

# FairEmail
eu.faircode.email_LATEST.apk|FDROIDREPO|app|FairEmail.apk;PRESIGNED|true

# Fennec Browser
org.mozilla.fennec_fdroid_LATEST.apk|FDROIDREPO|app|Fennec.apk;PRESIGNED|true

# QKSMS Messaging
com.moez.QKSMS_LATEST.apk|FDROIDREPO|app|QKSMS.apk;PRESIGNED|true

# FFUpdater
# Allow usesr to pick a Firefox-based browser
de.marmaro.krt.ffupdater_LATEST.apk|FDROIDREPO|app|FFUpdater.apk;PRESIGNED|true

# ICSx5 
at.bitfire.icsdroid_LATEST.apk|FDROIDREPO|app|ICSx5.apk;PRESIGNED|true

# DAVx5
at.bitfire.davdroid_LATEST.apk|FDROIDREPO|app|DAVx5.apk;PRESIGNED|true

# NextCloud Sync client
com.nextcloud.client_LATEST.apk|FDROIDREPO|app|NextCloud.apk;PRESIGNED|true

##########################################################################################################
# OFFICE
##########################################################################################################

# Etar Calendar
ws.xsoh.etar_LATEST.apk|FDROIDREPO|app|Etar.apk;PRESIGNED|true

# OpenTasks
org.dmfs.tasks_LATEST.apk|FDROIDREPO|app|OpenTasks.apk;PRESIGNED|true

# NextCloudNotes
it.niedermann.owncloud.notes_LATEST.apk|FDROIDREPO|app|NextCloudNotes.apk;PRESIGNED|true

